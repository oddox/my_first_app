import React, { Component } from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  Redirect
} from "react-router-dom";

import { Provider } from "react-redux";

import SignIn from "./pages/SignIn";
import Dashboard from "./pages/Dashboard";
import Posts from "./pages/Posts";
import Photos from "./pages/Photos";
import Products from "./pages/Products";
import ClassComponent from "./pages/ClassComponent";
import Functional from "./pages/Functional";
import Hello from "./pages/Hello";
import Hello2 from "./pages/Hello2";

import store from "./redux/store";

import "bootstrap/dist/css/bootstrap.min.css";

import Cookies from "universal-cookie";
const cookies = new Cookies();

const PrivateRoute = ({ children, ...rest }) => {
  return (
    <Route
      {...rest}
      render={() =>
        cookies.get("_token") ? (
          children
        ) : (
          <Redirect
            to={{
              pathname: "/"
            }}
          />
        )
      }
    />
  );
};

export default class App extends Component {
  componentDidMount() {
    console.log(process.env);
  }
  render() {
    return (
      <Provider store={store}>
        <Router>
          <div>
            <Switch>
              {/* register private dashboard route */}
              <PrivateRoute path="/dashboard" exact>
                <Dashboard />
              </PrivateRoute>

              {/* register private posts route */}
              <PrivateRoute path="/posts" exact>
                <Posts />
              </PrivateRoute>

              {/* register private photos route */}
              <PrivateRoute path="/photos" exact>
                <Photos />
              </PrivateRoute>

              {/* register private products route */}
              <PrivateRoute path="/products" exact>
                <Products />
              </PrivateRoute>

              {/* register public signin route */}
              <Route path="/" exact>
                <SignIn />
              </Route>

              {/* register public hello2 route */}
              <Route path="/hello2/:slug" component={Hello2} exact />
            </Switch>
          </div>
        </Router>
      </Provider>
    );
  }
}
