import { GET_POSTS } from "./type";
import axios from "axios";

export const getPost = () => dispatch => {
  //"https://jsonplaceholder.typicode.com/" + "posts"
  axios
    .get(process.env.REACT_APP_API + "posts")
    .then(function(response) {
      dispatch({
        type: GET_POSTS,
        payload: response.data
      });
    })
    .catch(function(error) {
      // handle error
      console.log(error);
    })
    .finally(function() {
      // always executed
    });
};

export const login = () => dispatch => {
  axios
    .get(process.env.REACT_APP_LOGIN_API + "posts")
    .then(function(response) {
      dispatch({
        type: GET_POSTS,
        payload: response.data
      });
    })
    .catch(function(error) {
      // handle error
      console.log(error);
    })
    .finally(function() {
      // always executed
    });
};
