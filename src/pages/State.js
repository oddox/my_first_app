import React, { Component } from "react";
import { Button } from "react-bootstrap";

class State extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: props.name ? props.name : "Ali"
    };
  }

  componentDidMount() {
    let _this = this;
    setTimeout(function() {
      _this.setState({ number: 300, nama: "Abu" });
    }, 3000);
  }

  render() {
    const number = this.props.data.map(item => <div>{item.nama}</div>);
    return (
      <div>
        <Button>Button</Button>
      </div>
    );
  }
}

export default State;
