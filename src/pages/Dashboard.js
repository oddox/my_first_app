import React, { Component } from "react";
import { Container, Row, Col, Navbar, Nav, NavDropdown } from "react-bootstrap";

import DashboardCard from "./components/DashboardCard";
import Navs from "./components/Navs";

import axios from "axios";

class Dashboard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      posts: [],
      comments: [],
      albums: [],
      photos: []
    };
  }

  componentDidMount() {
    this.getPosts();
    this.getComments();
    this.getAlbums();
    this.getPhotos();
  }

  getPosts = () => {
    let _this = this;
    axios
      .get("https://jsonplaceholder.typicode.com/posts")
      .then(function(response) {
        _this.setState({
          posts: response.data
        });
      })
      .catch(function(error) {
        // handle error
        console.log(error);
      })
      .finally(function() {
        // always executed
      });
  };

  getComments = () => {
    let _this = this;
    axios
      .get("https://jsonplaceholder.typicode.com/comments")
      .then(function(response) {
        _this.setState({
          comments: response.data
        });
      })
      .catch(function(error) {
        // handle error
        console.log(error);
      })
      .finally(function() {
        // always executed
      });
  };

  getAlbums = () => {
    let _this = this;
    axios
      .get("https://jsonplaceholder.typicode.com/albums")
      .then(function(response) {
        _this.setState({
          albums: response.data
        });
      })
      .catch(function(error) {
        // handle error
        console.log(error);
      })
      .finally(function() {
        // always executed
      });
  };

  getPhotos = () => {
    let _this = this;
    axios
      .get("https://jsonplaceholder.typicode.com/photos")
      .then(function(response) {
        _this.setState({
          photos: response.data
        });
      })
      .catch(function(error) {
        // handle error
        console.log(error);
      })
      .finally(function() {
        // always executed
      });
  };

  render() {
    return (
      <div>
        <Navs />
        <h3 style={{ margin: 20 }}>Dashboard</h3>
        <Container>
          <Row>
            <Col md={3}>
              <DashboardCard numbers={this.state.posts.length} title="Posts" />
            </Col>
            <Col md={3}>
              <DashboardCard
                numbers={this.state.comments.length}
                title="Comments"
              />
            </Col>
            <Col md={3}>
              <DashboardCard
                numbers={this.state.albums.length}
                title="Albums"
              />
            </Col>
            <Col md={3}>
              <DashboardCard
                numbers={this.state.photos.length}
                title="Photos"
              />
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

export default Dashboard;
