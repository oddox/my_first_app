import React, { Component } from "react";

import { Card, Button, Container, Row, Col, Spinner } from "react-bootstrap";

export default class DashboardCard extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <Card style={{ padding: 20 }}>
        <Card.Body>
          <Card.Title>
            {this.props.numbers > 0 ? (
              this.props.numbers
            ) : (
              <Spinner animation="border" role="status"></Spinner>
            )}
          </Card.Title>
          <Card.Text>{this.props.title}</Card.Text>
        </Card.Body>
      </Card>
    );
  }
}
