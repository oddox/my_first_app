import React, { Component } from "react";

export default class ClassComponent extends Component {
  constructor(props) {
    super(props);

    this.state = {
      number: 0
    };
  }

  handleIncrease = () => {
    this.setState({ number: this.state.number + 1 });
  };

  render() {
    return (
      <div>
        <div>Number: {this.state.number}</div>
        <button onClick={this.handleIncrease}>Increase</button>
      </div>
    );
  }
}
