import React, { useState } from "react";

export default function Functional() {
  const [number, setNumber] = useState(0);

  const increase = () => {
    setNumber(prevState => prevState + 1);
  };

  const decrease = () => {
    setNumber(prevState => prevState - 1);
  };

  return (
    <div>
      <div>Number: {number}</div>
      <button onClick={increase}>Increase</button>
      <button onClick={decrease}>Decrease</button>
    </div>
  );
}
