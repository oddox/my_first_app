import React, { Component } from "react";
import Navs from "./components/Navs";

import { Card, Container } from "react-bootstrap";

import { connect } from "react-redux";

import { getPost } from "../redux/actions/postActions";

import axios from "axios";

class Posts extends Component {
  componentDidMount() {
    this.props.getPost();
  }

  render() {
    return (
      <div>
        <Navs />
        <Container style={{ paddingTop: 20 }}>
          {console.log(this.props)}
          {this.props.posts.posts.map((item, index) => {
            return (
              <Card style={{ marginBottom: 10 }} key={index}>
                <Card.Body>
                  <Card.Title>{item.title}</Card.Title>
                  <Card.Text>{item.body}</Card.Text>
                </Card.Body>
              </Card>
            );
          })}
        </Container>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  posts: state.posts
});

const mapDispatchToProps = {
  getPost
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Posts);
