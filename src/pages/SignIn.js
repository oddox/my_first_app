import React, { Component } from "react";

import {
  Navbar,
  Card,
  Button,
  Container,
  Row,
  Form,
  Col,
  Spinner,
  Alert
} from "react-bootstrap";

import { withRouter } from "react-router-dom";

import Cookies from "universal-cookie"; //

import axios from "axios";

const cookies = new Cookies(); //

class SignIn extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
      error: false,
      email: "",
      password: ""
    };
  }

  componentDidMount() {
    if (cookies.get("_token")) {
      this.props.history.push("/dashboard");
    }
  }

  signIn = () => {
    this.setState({
      isLoading: true
    });

    let _this = this;
    axios
      .post("https://reqres.in/api/login", {
        email: this.state.email,
        password: this.state.password
      })
      .then(function(response) {
        console.log(response);
        _this.setState({
          isLoading: false,
          error: false
        });
        cookies.set("_token", response.data.token); //
        _this.props.history.push("/dashboard");
      })
      .catch(function(error) {
        console.log(error.response);

        _this.setState({
          isLoading: false,
          error: true
        });
      });
  };

  render() {
    return (
      <div>
        <Navbar bg="light">
          <Navbar.Brand href="#">My App</Navbar.Brand>
        </Navbar>
        <Container>
          <Row
            style={{
              flex: 1,
              justifyContent: "center",
              alignItems: "center",
              height: "100vh"
            }}
          >
            <Col md="6">
              {this.state.error ? (
                <Alert variant={"danger"}>Wrong username or password!</Alert>
              ) : null}
              <Card>
                <Card.Header>Sign In</Card.Header>
                <Card.Body>
                  <Card.Title>Please sign in to continue</Card.Title>
                  <Form>
                    <Form.Group controlId="formBasicEmail">
                      <Form.Label>Email address</Form.Label>
                      <Form.Control
                        type="email"
                        placeholder="Enter email"
                        onChange={e => this.setState({ email: e.target.value })}
                      />
                    </Form.Group>

                    <Form.Group controlId="formBasicPassword">
                      <Form.Label>Password</Form.Label>
                      <Form.Control
                        type="password"
                        placeholder="Password"
                        onChange={e =>
                          this.setState({ password: e.target.value })
                        }
                      />
                    </Form.Group>
                  </Form>
                  <Button variant="primary" onClick={this.signIn}>
                    {this.state.isLoading ? (
                      <Spinner
                        animation="border"
                        size="sm"
                        role="status"
                      ></Spinner>
                    ) : (
                      "Sign In"
                    )}
                  </Button>
                </Card.Body>
              </Card>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

export default withRouter(SignIn);
