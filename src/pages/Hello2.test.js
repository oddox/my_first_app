import React from "react";
import Hello2 from "./Hello2";
import renderer from "react-test-renderer";
import ReactDOM from "react-dom";

it("renders without crashing", () => {
  const div = document.createElement("div");
  ReactDOM.render(<Hello2 name="Ali" x />, div);
});
