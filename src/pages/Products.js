import React, { Component } from "react";
import axios from "axios";
import {
  Card,
  Container,
  Row,
  Col,
  Spinner,
  Button,
  Modal,
  Form,
  Alert
} from "react-bootstrap";

import { bahasa, api_url, english } from "../configs";

import Navs from "./components/Navs";

class Products extends Component {
  constructor(props) {
    super(props);
    this.state = {
      products: {
        data: []
      },
      pageNumber: 1,
      isLoading: true,
      isShow: false,
      name: "",
      description: "",
      errorName: false,
      errorDesc: false,
      errors: {
        name: null,
        description: null
      }
    };
  }

  componentDidMount() {
    this.getProducts();
  }

  nextPage = () => {
    let newProducts = {
      data: []
    };
    this.setState(
      { pageNumber: this.state.pageNumber + 1, products: newProducts },
      () => {
        this.getProducts();
      }
    );
  };

  prevPage = () => {
    let newProducts = {
      data: []
    };
    this.setState(
      { pageNumber: this.state.pageNumber - 1, products: newProducts },
      () => {
        this.getProducts();
      }
    );
  };

  getProducts = page => {
    let _this = this;
    axios
      .get(api_url + "products?page=" + this.state.pageNumber)
      .then(function(response) {
        _this.setState({
          products: response.data
        });
      })
      .catch(function(error) {
        // handle error
        console.log(error);
      })
      .finally(function() {
        _this.setState({
          isLoading: false
        });
      });
  };

  handleClose = () => {
    this.setState({
      isShow: false
    });
  };

  handleOpen = () => {
    this.setState({
      isShow: true
    });
  };

  handleSubmit = () => {
    let _this = this;

    let fields = { name: this.state.name, description: this.state.description };

    if (!this.handleValidation(fields)) {
      return null;
    }

    axios
      .post("http://192.168.4.21/bumipro/public/api/products", {
        name: this.state.name,
        description: this.state.description
      })
      .then(function(response) {
        alert("success");
        _this.handleClose();
        _this.getProducts();
        _this.setState({
          name: "",
          description: ""
        });
      })
      .catch(function(error) {
        console.log(error);
      });
  };

  handleDelete = productId => {
    let _this = this;
    axios
      .delete("http://192.168.4.21/bumipro/public/api/products/" + productId)
      .then(function(response) {
        _this.getProducts();
        console.log(response.data);
      })
      .catch(function(error) {
        // handle error
        console.log(error);
      });
  };

  handleValidation = fields => {
    let errors = {};

    if (fields["name"] === "") {
      errors["name"] = {
        error: true,
        message: "Name is empty!"
      };
    }

    if (fields["description"] === "") {
      errors["description"] = {
        error: true,
        message: "Description is empty!"
      };
    }

    if (Object.keys(errors).length > 0) {
      this.setState({
        errors: errors
      });
      return false;
    }

    this.setState({
      errors: {}
    });

    return true;
  };

  render() {
    let renderList = this.state.products.data.map((item, index) => {
      return (
        <Col style={{ paddingTop: 10 }} md={3} key={index}>
          <Card>
            <Card.Img variant="top" src={item.cover_image} />
            <Card.Body>
              <Card.Title>{item.name}</Card.Title>

              <Button
                onClick={() => this.handleDelete(item.id)}
                variant="danger"
              >
                Delete
              </Button>
            </Card.Body>
          </Card>
        </Col>
      );
    });

    return (
      <div>
        <Navs />
        <Container style={{ paddingTop: 20 }}>
          <div>
            <Button
              onClick={this.handleOpen}
              className={{ float: "right" }}
              variant="primary"
            >
              {english.send}
            </Button>
          </div>
          <Row>
            {this.state.products.data.length > 0 ? (
              renderList
            ) : (
              <Spinner animation="border" role="status">
                <span className="sr-only">Loading...</span>
              </Spinner>
            )}
          </Row>
          {/* Prev */}
          {this.state.products.current_page !== 1 ? (
            <Button
              onClick={this.prevPage}
              style={{ margin: 20 }}
              variant="primary"
            >
              Prev
            </Button>
          ) : (
            <Button disabled style={{ margin: 20 }} variant="primary">
              Prev
            </Button>
          )}
          {/* Next */}
          {this.state.products.to === this.state.products.total ? (
            <Button disabled style={{ margin: 20 }} variant="primary">
              Next
            </Button>
          ) : (
            <Button
              style={{ margin: 20 }}
              onClick={this.nextPage}
              variant="primary"
            >
              Next
            </Button>
          )}
          <Modal show={this.state.isShow} onHide={this.handleClose}>
            <Modal.Header closeButton>
              <Modal.Title>Add new product</Modal.Title>
            </Modal.Header>
            <Modal.Body>
              {Object.keys(this.state.errors).length > 0 ? (
                <Alert variant={"warning"}>
                  Error
                  <br />
                  {this.state.errors.name ? this.state.errors.name.message : ""}
                  <br />
                  {this.state.errors.description
                    ? this.state.errors.description.message
                    : ""}
                </Alert>
              ) : (
                ""
              )}

              <Form>
                <Form.Group controlId="formBasicEmail">
                  <Form.Label>Product Name</Form.Label>
                  <Form.Control
                    type="text"
                    placeholder="Product Name"
                    onChange={e => this.setState({ name: e.target.value })}
                  />
                </Form.Group>

                <Form.Group controlId="formBasicPassword">
                  <Form.Label>Description</Form.Label>
                  <Form.Control
                    type="text"
                    placeholder="Description"
                    onChange={e =>
                      this.setState({ description: e.target.value })
                    }
                  />
                </Form.Group>
              </Form>
            </Modal.Body>
            <Modal.Footer>
              <Button variant="secondary" onClick={this.handleClose}>
                Close
              </Button>
              <Button variant="primary" onClick={this.handleSubmit}>
                Add
              </Button>
            </Modal.Footer>
          </Modal>
        </Container>
      </div>
    );
  }
}

export default Products;
