import React, { Component } from "react";
import axios from "axios";
import { Card, Container, Row, Col, Spinner } from "react-bootstrap";

import Navs from "./components/Navs";

class Photos extends Component {
  constructor(props) {
    super(props);
    this.state = {
      photos: [],
      isLoading: true
    };
  }

  componentDidMount() {
    this.getPhotos();
  }

  getPhotos = () => {
    let _this = this;
    axios
      .get("https://jsonplaceholder.typicode.com/photos")
      .then(function(response) {
        _this.setState({
          photos: response.data
        });
      })
      .catch(function(error) {
        // handle error
        console.log(error);
      })
      .finally(function() {
        _this.setState({
          isLoading: false
        });
      });
  };

  render() {
    let renderList = this.state.photos.map((item, index) => {
      return (
        <Col style={{ paddingTop: 10 }} md={3} key={index}>
          <Card>
            <Card.Img variant="top" src={item.thumbnailUrl} />
            <Card.Body>
              <Card.Title>{item.title}</Card.Title>
            </Card.Body>
          </Card>
        </Col>
      );
    });

    return (
      <div>
        <Navs />
        <Container style={{ paddingTop: 20 }}>
          <Row>
            {this.state.photos.length > 0 ? (
              renderList
            ) : (
              <Spinner animation="border" role="status">
                <span className="sr-only">Loading...</span>
              </Spinner>
            )}
          </Row>
        </Container>
      </div>
    );
  }
}

export default Photos;
